package krio.draw;

import krio.physics.VertletParticle;
import static krio.util.Clip.clip;

import java.awt.*;

public class ParticleDrawer {
    public static void draw(VertletParticle p, Graphics g) {
        /*g.setColor(p.color);
        g.fillOval((int)(p.currentPos.x - p.rad), (int)(p.currentPos.y - p.rad), (int)(p.rad * 2), (int)(p.rad * 2));
        g.setColor(Color.LIGHT_GRAY.darker().darker());
        g.drawOval((int)(p.currentPos.x - p.rad), (int)(p.currentPos.y - p.rad), (int)(p.rad * 2), (int)(p.rad * 2));*/
        //((Graphics2D)g).setStroke(new BasicStroke(1));
        //g.fillOval((int)(p.currentPos.x - p.rad), (int)(p.currentPos.y - p.rad), (int)(p.rad * 2), (int)(p.rad * 2));
        g.setColor(Color.darkGray);
        p.links.forEach((l) -> {
            //g.setColor(Color.LIGHT_GRAY);
            /*float red = clip((float)l.weight * 2 - 1, 0f, 1f);
            float green = clip(0.8f * ((float)(-15 * (Math.pow(l.weight - 0.5, 2)) + 1)), 0f, 1f);
            float blue = clip((float)l.weight * -2 + 1, 0f, 1f);
            //g.setColor(new Color(red, green, blue, 1.0f));
            */
            g.drawLine((int)l.a.currentPos.x, (int)l.a.currentPos.y, (int)l.b.currentPos.x, (int)l.b.currentPos.y);
        });
    }
}
