package krio.draw;

import java.awt.*;

import krio.main.MyPanel;
import krio.world.World;

public class WorldDrawer {
    public static void draw(World w, Graphics g) {
        //g.setColor(Color.lightGray.darker().darker());
        //g.fillOval(MyPanel.WIDTH/2 - 300, MyPanel.HEIGHT/2 - 300, 600, 600);
        w.particles.forEach((p) -> ParticleDrawer.draw(p, g));
    }
}
