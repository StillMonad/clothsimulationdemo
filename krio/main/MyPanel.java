package krio.main;

import krio.draw.WorldDrawer;
import krio.physics.VertletParticle;
import krio.util.Vec2;
import krio.world.World;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.LinkedList;
import javax.swing.*;


/**
 * Redraws itself by method paint() with given fps
 */
public class MyPanel extends JPanel implements ActionListener {

    /** Window width */
    public static int WIDTH  = 800;
    /** Window height */
    public static int HEIGHT = 900;
    /** Mouse coordinates inside the window */
    Point mouseCoords = new Point(0,0);
    boolean cuttingOn = false;
    /** Timer, calling repaint() with given frequency */
    Timer timer;
    /** Frames per second */
    int fps = 31;
    /** Variable used for calculating frames per second, refreshes every frame */
    long start = 0;
    /** Variable used for calculating frames per second, refreshes every frame  */
    long elapsed = 1;
    /** Variable containing average fps for the last few frames, refreshes every frame  */
    double smoothFpsCount = fps;
    /** holding all the particles and handling physics */
    World world = new World();

    public MyPanel() {
        setPreferredSize(new Dimension(WIDTH, HEIGHT));
        timer = new Timer(1000 / fps, this);

        this.setBackground(Color.LIGHT_GRAY);
        //this.setBackground(Color.WHITE);

        this.addMouseMotionListener(new MouseMotionAdapter() {
            public void mouseMoved(MouseEvent me)
            {
                mouseCoords.x = me.getX();
                mouseCoords.y = me.getY();
            }
        });

        this.addMouseListener(new MouseListener() {
            public void mousePressed(MouseEvent me) {}
            public void mouseReleased(MouseEvent me) { }
            public void mouseEntered(MouseEvent me) { }
            public void mouseExited(MouseEvent me) { }
            public void mouseClicked(MouseEvent me) {
                cuttingOn = !cuttingOn;
            }
        });

        timer.start();
    }

    /**
     * called with every timer tick,
     * calling repaint()
     * */
    @Override
    public void actionPerformed(ActionEvent a) {

        repaint();
        world.run(1.0);
        Vec2 mouse = new Vec2(mouseCoords.x, mouseCoords.y);

        if (cuttingOn) {
            for (VertletParticle p : world.particles) {
                if (Vec2.len(Vec2.sub(p.currentPos, mouse)) < 5) p.links = new LinkedList<>();
            }
        }

        //System.out.println(cuttingOn);
    }

    /**
     * Method paint() is called inside repaint()
     * */
    @Override
    public void paint(Graphics gr) {
        super.paint(gr);
        Graphics2D g2d = (Graphics2D) gr;
        ((Graphics2D) gr).setStroke(new BasicStroke(1));
        WorldDrawer.draw(world, gr);
        g2d.setPaint(Color.WHITE);
        smoothFpsCount = smoothFpsCount * 0.8 + 0.2 * 1_000_000_000.0 / ((double)elapsed);
        g2d.drawString("real fps: " + 1_000_000_000.0 / ((double)elapsed), 30, 20);
        g2d.drawString("average fps: " + smoothFpsCount, 30, 40);
        elapsed = System.nanoTime() - start;
        start = System.nanoTime();
    }
}
