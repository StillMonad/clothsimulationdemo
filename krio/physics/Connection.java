package krio.physics;

import krio.util.Vec2;
import static krio.util.Clip.clip;

public class Connection {
    public VertletParticle a;
    public VertletParticle b;
    public double maxDistance;
    public double strength = 0.4;
    public double stress = 0;
    public double weightColor; //just for coloring

    public Connection(VertletParticle a, VertletParticle b, double maxDistance) {
        this.a = a;
        this.b = b;
        this.maxDistance = maxDistance;
    }

    public boolean applyConnection() {
        double distance = Vec2.len(Vec2.sub(a.currentPos, b.currentPos));
        stress += 0 <= (distance * (1 - strength) - maxDistance) * (1 - strength) ? (distance - maxDistance) * 1 : -1.0;
        stress = Math.max(0, stress);

        weightColor = clip((distance - maxDistance), 0.0, 1.0);
        //System.out.println(maxDistance / distance);

        if (stress > 1) return false;
        //if(distance >  + maxDistance) return  false;
        //if(distance > 15 * (a.currentPos.y / 1200) + maxDistance) return  false;

        Physics.applyConstraintsDistance(a, b, maxDistance * 0.0, maxDistance * 1.1, 0.5, 1.0);
        return true;
    }
}
