package krio.physics;

import krio.main.MyPanel;
import krio.util.Vec2;

import java.util.ArrayList;

public class Physics {
    public static void applyConstraintsDistance(VertletParticle a, VertletParticle b, double min, double max, double weight, double power) {
        Vec2 dirToA = Vec2.sub(a.currentPos, b.currentPos);
        double distance = Vec2.len(dirToA);

        if (a.positionLocked)
            weight = 0;
        else if (b.positionLocked)
            weight = 1;

        if (distance < min) {
            double difference = (min - distance);
            dirToA = Vec2.normalize(dirToA);
            a.currentPos = Vec2.add(a.currentPos, Vec2.mul(dirToA,  difference * weight * power));
            b.currentPos = Vec2.add(b.currentPos, Vec2.mul(dirToA, -difference * (1 - weight) * power));
        } else if (distance > max) {
            double difference = (max - distance);
            dirToA = Vec2.normalize(dirToA);
            a.currentPos = Vec2.add(a.currentPos, Vec2.mul(dirToA,  difference * weight));
            b.currentPos = Vec2.add(b.currentPos, Vec2.mul(dirToA, -difference * (1 - weight)));
        }
    }

    public static void applyConstraintsDistance(VertletParticle a, VertletParticle b, double min, double max, double weight) {
        applyConstraintsDistance(a, b, min, max, 0.5, 1);
    }

    public static void applyConstraintsDistance(VertletParticle a, VertletParticle b, double min, double max) {
        applyConstraintsDistance(a, b, min, max, 0.5);
    }

    public static void applyConstraintsCircle(VertletParticle a, double rad) {
        applyConstraintsDistance(a, new VertletParticle(MyPanel.WIDTH / 2, MyPanel.HEIGHT / 2), 0, rad - a.rad, 1);
    }

    public static void accelerate(VertletParticle p, Vec2 acc) {
        p.acc = Vec2.add(p.acc, acc);
    }
}
