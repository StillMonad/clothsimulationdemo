package krio.physics;

import krio.util.GenerateRandom;
import krio.util.Vec2;

import java.awt.*;
import java.util.ArrayList;
import java.util.LinkedList;

public class VertletParticle {
    public Vec2 currentPos;
    public Vec2 previousPos;
    public Vec2 acc = new Vec2();
    public double rad = 10;
    public boolean positionLocked = false;
    /*public Color color = new Color(
            (float)GenerateRandom.getRandomDouble(0,1),
            (float)GenerateRandom.getRandomDouble(0,1),
            (float)GenerateRandom.getRandomDouble(0,1),
            (float)GenerateRandom.getRandomDouble(0,1)
    );*/
    public Color color = Color.LIGHT_GRAY;

    public LinkedList<Connection> links = new LinkedList<>();

    public VertletParticle(double x, double y, double rad) {
        currentPos = new Vec2(x, y);
        previousPos = new Vec2(x, y);
        this.rad = rad;
    }
    public VertletParticle(double x, double y) {
        this(x, y, GenerateRandom.getRandomDouble(5, 15));
    }
    /** currentPos = 2 * currentPos - previousPos + acc * dt^2, applies  */
    public void move(double dt) {
        Vec2 temp = new Vec2(currentPos.x, currentPos.y);
        currentPos = Vec2.add(Vec2.sub(Vec2.mul(currentPos, 2), previousPos), Vec2.mul(acc, dt*dt));
        previousPos = temp;
        acc = new Vec2();
    }
}
