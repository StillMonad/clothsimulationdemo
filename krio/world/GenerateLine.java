package krio.world;

import krio.physics.Connection;
import krio.physics.VertletParticle;
import krio.util.Vec2;

import java.util.ArrayList;

public class GenerateLine {
    public static ArrayList<VertletParticle> generate(Vec2 start, Vec2 end, int count, double rad) {
        /** step increment: inc = (end - start) * (1 / (count - 1)); */
        Vec2 inc = Vec2.mul(Vec2.sub(end, start), 1d / (count - 1d));
        ArrayList<VertletParticle> line = new ArrayList<>();

        VertletParticle p = new VertletParticle(0,0);
        VertletParticle t = new VertletParticle(0,0);

        for (int i = 0; i < count; ++i) {
            /** new particle position: pos = (inc * i) + start; */
            Vec2 pos = Vec2.add(Vec2.mul(inc, i), start);
            p = new VertletParticle(pos.x, pos.y, rad);
            if (i != 0) p.links.add(new Connection(p, t, Vec2.len(inc)));
            t = p;
            line.add(p);
        }
        return line;
    }
}
