package krio.world;

import krio.physics.Connection;
import krio.physics.VertletParticle;
import krio.util.Vec2;

import java.util.ArrayList;

public class GenerateRectangle {
    public static ArrayList<VertletParticle> generate(Vec2 tl, Vec2 tr, Vec2 bl, int countW, int countH, double rad) {
        ArrayList<VertletParticle> curr = new ArrayList<>();
        ArrayList<VertletParticle> prev = new ArrayList<>();
        ArrayList<VertletParticle> rect = new ArrayList<>();

        /** step increment: inc = (end - start) * (1 / (count - 1)); */
        Vec2 inc = Vec2.mul(Vec2.sub(bl, tl), 1d / (countH - 1d));

        for(int i = 0; i < countH; ++i) {
            Vec2 shift = Vec2.mul(inc, i);
            curr = GenerateLine.generate(Vec2.add(tl, shift), Vec2.add(tr, shift), countW, rad);
            if (i != 0) {
                for (int j = 0; j < countW; ++j) {
                    curr.get(j).links.add(new Connection(curr.get(j), prev.get(j), Vec2.len(inc)));
                }
            }
            prev = curr;
            curr.forEach((p) -> rect.add(p));
        }

        return rect;
    }
}
