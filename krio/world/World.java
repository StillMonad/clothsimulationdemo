package krio.world;

import krio.main.MyPanel;
import krio.physics.Connection;
import krio.physics.Physics;
import krio.physics.VertletParticle;
import static krio.util.GenerateRandom.getRandomDouble;
import krio.util.Vec2;

import java.util.ArrayList;
import java.util.LinkedList;

public class World {
    /** particles count */
    public final int count = 0;
    /** contains all the particles in the world*/
    public LinkedList<VertletParticle> particles = new LinkedList<>();
    /** gravity */
    public Vec2 G = new Vec2(0, 0.6);
    /** subiterations of physics inside one world.run(dt) call */
    public int reaclcCount = 15;
    /** flag enabling collision solving - affects performance massively */
    public boolean solveCollisions = false;

    public World() {
        for(int i = 0; i < count; ++i) particles.add(new VertletParticle(
                        getRandomDouble(0, MyPanel.WIDTH),
                        getRandomDouble(0, MyPanel.HEIGHT), 10)
        );
        /*VertletParticle buffer = new VertletParticle(0,0);
        for (int i = 0; i < 60; ++i) {
            p = new VertletParticle(250 + i * 6,300, 3);
            if (i != 0) p.links.add(new Connection(p, buffer, 10));
            if (i == 30) p.positionLocked = true;
            particles.add(p);
            buffer = p;
        }*/

        //GenerateLine.generate(new Vec2(300, 350), new Vec2(500, 350), 5, 10).forEach((p) -> particles.add(p));
        //VertletParticle p;
        //p = new VertletParticle(350,500, 40);
        //p.positionLocked = true;
        //particles.add(p);
        GenerateRectangle.generate(
                new Vec2(200, 100),
                new Vec2(600, 100),
                new Vec2(200, 500),
                70,
                70,
                0)
                .forEach((p) -> particles.add(p));

        for (int i = 0; i < 70; ++i)
            if (i % 4 == 0) particles.get(i).positionLocked = true;

        //VertletParticle p = new VertletParticle(400,700, 40);
        //p.positionLocked = true;
        //particles.add(p);
    }

    public void run(double dt) {
        /** remove particles with y < 0 */
        /*for (int i = 0; i < particles.size(); ++i) {
            if (particles.get(i).currentPos.y < 0) particles.remove(i);
        }*/

        /** add new line of particles */
        /*for (int i = 0; i < 80; ++i) {
            particles.get(particles.size() - 1 - i).positionLocked = false;
        }
        GenerateLine.generate(new Vec2(200, 800), new Vec2(600, 800), 80, 0).forEach((p) -> particles.add(p));
        for (int i = 0; i < 80; ++i) {
            particles.get(particles.size() - 1 - i).positionLocked = true;
            particles.get(particles.size() - 1 - i).links.add(
                    new Connection(particles.get(particles.size() - 1 - i),
                            particles.get(particles.size() - 81 - i),
                            15)
            );
        }*/

        for (int i = 0; i < reaclcCount; ++i) {
            particles.forEach((p) -> {
                Physics.accelerate(p, G);
                if (!p.positionLocked) {
                    p.move(dt / reaclcCount);
                    //Physics.applyConstraintsCircle(p, 300);
                }
                p.links.forEach((l) -> {
                    boolean tryConnect = l.applyConnection();
                    if (!tryConnect) p.links.remove(l);
                });
                if (solveCollisions) {
                    particles.forEach((p2) -> {
                        if (p != p2) {
                            Physics.applyConstraintsDistance(p, p2, p.rad + p2.rad, Double.MAX_VALUE);
                        }
                    });
                }
            });
        }
    }
}
